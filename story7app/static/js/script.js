$(document).ready(function(){
    $('.toggle').click(function(){
        $('.toggle').toggleClass('active')
        $('body').toggleClass('night')
    })
})

$(document).ready(function(){
	$(".set > a").on("click", function(){
		if($(this).hasClass('active')){
			$(this).removeClass('active');
			$(this).siblings('.konten').slideUp(200);
			$(".set > a i").removeClass("fa-minus").addClass("fa-plus");
		}else{
			$(".set > a i").removeClass("fa-minus").addClass("fa-plus");
			$(this).find("i").removeClass("fa-minus").addClass("fa-minus");
			$(".set > a").removeClass("active");
			$(this).addClass("active");
			$('.konten').slideUp(200);
			$(this).siblings('.konten').slideDown(200);
		}
	});
});