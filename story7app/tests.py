from django.test import TestCase, Client
from django.urls import resolve
from .views import homepage

# Create your tests here.

class Story7Test(TestCase):
    def test_story_7_using_homepage_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'homepage.html')
    
    def test_story_7_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_story_7_using_homepage_func(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage)
    
    def test_story_7_text_is_exist(self):
        response = Client().get('/')
        self.assertContains(response, "Halo semuaaaa!!")
